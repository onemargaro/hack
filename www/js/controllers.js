// initialize parse application
Parse.initialize("CEWrW9rgUdhsOl9LK75Jc64H8EwdULUO4bHneutw", "4zi6TujQWQQkQtzyiLolKDB4Nu6ihBbQegHOaJGE");

angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $location) {

  // With  the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  // VISTA DE INDEX DE ROMMIES
  var Rommies = Parse.Object.extend("Rommie");
  var query = new Parse.Query(Rommies);
  $scope.baseRommies={};
  // delimito a que cargue solo 10
  query.limit(10);
  query.find({
    success: function(Rommies_base) {
      console.log(Rommies_base);
      $scope.baseRommies=Rommies_base;
    },
    error: function(error) {
      $scope.error=error;
    }
  });







  // prototipos para el login, el registro, agregar cuarto, posicion
  $scope.loginData = {
  }
  $scope.registerData={
  }
  $scope.addroom={
  }
  $scope.position={
  }

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modalLogin = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modalLogin.hide();
  };

  // Open the login modal with login
  $scope.login = function() {
    $scope.modalLogin.show();
  };
  // se quita el logeo
  $scope.logout = function() {
    if (Parse.User.current()) {
        Parse.User.logOut();
        $scope.firmado=false;
        $location.path("/");
    }
  };
  // modal de registro de usuarios
  $ionicModal.fromTemplateUrl('templates/register.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modalRegister = modal;
  });
  // Triggered in the registro modal to close it
  $scope.closeRegister = function() {
    $scope.modalRegister.hide();
  };

  // Open the register modal
  $scope.register = function() {
    $scope.modalRegister.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    // console.log('Doing login', $scope.loginData);
    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    Parse.User.logIn($scope.loginData.username, $scope.loginData.password, {
    success: function(user) {
        // Do stuff after successful login.
        $scope.firmado=true;
        Materialize.toast('Bienvenido: '+$scope.loginData.username, 3000, 'rounded')
        $location.path("/index");        
    },
      error: function(user, error) {
        // The login failed. Check error to see why.
        Materialize.toast('Error: '+$scope.loginData.username, 3000, 'rounded')
      }
    });
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
  

  // Perform the register action when the user submits the login form
  $scope.doRegister = function() {
    // console.log('Doing register', $scope.registerData );
    // se crea una instancia de usuario de Parse
    var user = new Parse.User();
    // se establecen los valores
    user.set("username", $scope.registerData.username);
    user.set("password", $scope.registerData.password);
    user.set("email", $scope.registerData.email);
    user.signUp(null, {
        success: function(user) {
          Materialize.toast('Bienvenido: '+$scope.registerData.username, 3000, 'rounded');
          $scope.firmado = true;
        },
        error: function(user, error) {
          // Show the error message somewhere and let the user try again.
          // alert("Error: " + error.code + " " + error.message);
          Materialize.toast("Error: "+$scope.registerData.username+" "+error, 3000, 'rounded')
          $scope.firmado=false;
        }
    });

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeRegister();
    }, 1000);
  };

  $scope.showPosition=function(position) {
    $scope.position.latitude=position.coords.latitude;
    $scope.position.longitude=position.coords.longitude;
    $scope.doAddroom(0);
  }
  $scope.doAddroom = function(on) {
    if (on) {
      if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition($scope.showPosition);
      } else { 
          console.log("Geolocation is not supported by this browser.");
      }
      return 0;
    }
    // console.log($scope.position.latitude);
    // console.log($scope.position.longitude);
    var fileUploadControl = $("#roomiefoto")[0];

    // console.log(Parse.User.current().id);
    if (Parse.User.current()) {
      var rommiesavailable = Parse.Object.extend("Rommie");
      var room = new rommiesavailable();
      if (fileUploadControl.files.length > 0) {
         var file = fileUploadControl.files[0];
         var name = "imagen.png";
         var file = new Parse.File(name, file);
      }
      var point = new Parse.GeoPoint({ latitude:$scope.position.latitude, longitude:$scope.position.longitude })
      room.set("imagen", file);
      room.set("tamanio",$scope.addroom.size);
      room.set("titulo",$scope.addroom.titulo);
      room.set("precio",$scope.addroom.price);
      room.set("descripcion",$scope.addroom.description);
      room.set("location",point);
      room.set("user_id",Parse.User.current().id)
      room.save(null,{
        success: function(room) {
           Materialize.toast('Insertado', 3000, 'rounded');
           var Rommies = Parse.Object.extend("Rommie");
            var query = new Parse.Query(Rommies);
            $scope.baseRommies={};
            // delimito a que cargue solo 10
            query.limit(10);
            query.find({
              success: function(Rommies_base) {
                console.log(Rommies_base);
                $scope.baseRommies=Rommies_base;
              },
              error: function(error) {
                $scope.error=error;
              }
            });
           $location.path("/index")
        },
        error: function(room, error) {
          Materialize.toast('Error: no se pudo insertar '+error, 3000, 'rounded');
        }
      });
      
    // console.log(fileUploadControl);
    }else{
      $location.path("/error-de-autenticacion");
    }
  };

  $scope.find = function() {
    var Rommie = Parse.Object.extend("Rommie");
    var query= new Parse.Query(Rommie);
    query.select("titulo","tamanio","precio","description","imagen");
    query.find
    $scope.buscar;
  }


})
.controller('PlaylistCtrl', function($scope, $stateParams) {
});